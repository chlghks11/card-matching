const backgroundBlur = document.querySelector(".board").firstElementChild;
const mainScreen = document.querySelector(".main-screen");
const startButton = document.querySelector(".start-button");
const gameScore = document.querySelector(".game-score");
const gameBoard = document.querySelector(".game-board");
const gameResultsScreen = document.querySelector(".game-results-screen");
const gameResults = document.querySelector(".game-results");
const restartButton = document.querySelector(".restart-button");
const settingButton = document.querySelector(".setting-button");
const settingScreen = document.querySelector(".setting-screen");

let numberOfCards = 16;  // 게임 카드 수 설정
const cardCheck = [];       // 오픈된 카드
let cardComparison = null;  // 각 카드의 matchingValue 값 비교용 변수
let foundScore = 0;          // 카드를 맞춘 갯수

function removeCard() {
  while (gameBoard.firstElementChild) {
    gameBoard.removeChild(gameBoard.firstElementChild);
  }
}

// 두번째 게임 재시작은 실행이 안된당....
// restartButton에 클릭 이벤트가 중복되서 그런가..?
function checkTheCardResults () {
  const fullMark = numberOfCards*5;  // 이번 게임 카드 수에 따른 총 점수
  if (foundScore === fullMark) {
    setTimeout(() => {
      foundScore = 0;
      backgroundBlur.classList.toggle("background-blur");
      gameResultsScreen.classList.toggle("display-hide");
    }, 1200);
  }
}

function checkTheCard (ev) {
  const cardStatus = ev.currentTarget.dataset.cardStatus;  // 카드 상태
  const matchingValue = ev.currentTarget.dataset.matchingValue; //
  if (cardStatus === "close") {
    if (cardComparison === null) {
      ev.currentTarget.classList.toggle("click-effect");
      ev.currentTarget.dataset.cardStatus = "open";
      cardCheck.push(ev.currentTarget);
      cardComparison = matchingValue;
    } else if (cardCheck.length < 2) {
      if (cardComparison === matchingValue) {
        ev.currentTarget.classList.toggle("click-effect");
        ev.currentTarget.dataset.cardStatus = "open";
        cardCheck.splice(0, 1);
        cardComparison = null;
        foundScore += 10;
        setTimeout(() => {gameScore.textContent = foundScore}, 500);
      } else {
        ev.currentTarget.classList.toggle("click-effect");
        cardCheck.push(ev.currentTarget);
        setTimeout(() => {
          cardCheck.forEach(element => {
            element.classList.toggle("click-effect");
          });
          cardCheck[0].dataset.cardStatus = "close";
          cardCheck.splice(0, 2);
          cardComparison = null;
        }, 800);
      }
    }
  }
  checkTheCardResults();
}

function flipAllCards (time) {
  const cardBox = document.querySelectorAll(".card-container");
  cardBox.forEach(card => card.classList.add('click-effect'));
  setTimeout(() => {
    cardBox.forEach(card => card.classList.remove('click-effect'));
  }, 1500);
}

function addEventAndValueToBox () {
  const duplicateCheck = [];  //중복 확인
  for (let i = 0; i < numberOfCards; i++) {
    const cardBox = document.querySelectorAll(".card-container")[i];
    const randomNumber =  Math.floor(Math.random()*numberOfCards)+1;
    // 중복 숫자 확인
    const checkForDuplicateNumbers = duplicateCheck.includes(randomNumber);
    if (checkForDuplicateNumbers) {
      i--;
      continue;
    }
    duplicateCheck.push(randomNumber);
    cardBox.dataset.cardStatus = "close";
    randomNumber % 2
    ? cardBox.dataset.matchingValue = (randomNumber + 1) / 2
    : cardBox.dataset.matchingValue = randomNumber / 2;
    const matchingValue = cardBox.dataset.matchingValue;
    cardBox.lastElementChild.src = `./images/food/${matchingValue}.png`;
    setTimeout(() => {
      cardBox.addEventListener("click", checkTheCard);
    }, 2000);
  }
  flipAllCards();
}

function createCard () {
  const div = document.createElement("div");
  const img1 = document.createElement("img");
  const img2 = document.createElement("img");
  div.classList.add("card-container");
  img1.classList.add("back");
  img2.classList.add("front");
  gameBoard.appendChild(div);
  div.appendChild(img1);
  div.appendChild(img2);
}

function startGame () {
  for (let i = 0; i < numberOfCards; i++) {
    createCard();
  }
  setTimeout(() => {
    gameScore.textContent = foundScore;
    settingButton.classList.toggle("display-hide");
    gameScore.classList.toggle("display-hide");
    mainScreen.classList.toggle("display-hide");
    gameBoard.classList.toggle("display-hide");
    addEventAndValueToBox();
  }, 400);
}

function createSettingScreen () {
  settingButton.addEventListener("click", () =>{
    settingScreen.classList.toggle("display-hide");
  });
  const addEventSelectButton = settingScreen.querySelectorAll("button")
  addEventSelectButton.forEach(element => {
    element.addEventListener("click",ev => {
      for (let i = 0; i < 3; i++) {
        const elementChild = ev.target.parentElement.querySelectorAll("button")[i];
        elementChild.dataset.select = "false";
      }
      ev.target.dataset.select = "true";
      numberOfCards = ev.target.dataset.setNumberOfCards;
      settingScreen.classList.toggle("display-hide");
    });
  });
}

function init () {
  createSettingScreen();
  startButton.addEventListener("click", startGame);
  restartButton.addEventListener("click", () => {
    setTimeout(() => {
      backgroundBlur.classList.toggle("background-blur");
      settingButton.classList.toggle("display-hide");
      gameScore.classList.toggle("display-hide");
      mainScreen.classList.toggle("display-hide");
      gameBoard.classList.toggle("display-hide");
      gameResultsScreen.classList.toggle("display-hide");
      removeCard();
    }, 800);
  });
}

init();
